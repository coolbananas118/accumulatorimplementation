package com.Matthew;

import org.junit.Before;
import org.junit.Test;

import java.time.LocalDateTime;
import java.time.Month;

import static org.junit.Assert.*;

/**
 * Created by matthew2chambers on 24/08/2016.
 */
public class AccumulatorTest {

    private Accumulator accumulator;

    @Before
    public void setup() {
        accumulator = new SimpleAccumulator();
    }

    @Test
    public void WHEN_no_accumulations_have_been_applied_THEN_the_initial_value_of_the_running_total_is_0() {

        // Verify
        assertEquals(0, accumulator.getTotal());
    }

    @Test
    public void WHEN_1_number_is_accumulated_THEN_the_accumulation_sum_AND_the_running_total_should_be_the_same_as_the_input_amount() {

        // Setup
        int input_amount = 44;

        // Exercise
        int firstSum = accumulator.accumulate(input_amount);
        int total = accumulator.getTotal();

        // Verify
        assertEquals(input_amount, firstSum);
        assertEquals(input_amount, total);
    }

    @Test
    public void WHEN_multiple_numbers_are_accumulated_once_THEN_the_accumulation_sum_AND_the_running_total_should_be_the_same() {

        // Setup
        int[] input_amount = {44,55,66};

        // Exercise
        int firstSum = accumulator.accumulate(input_amount);
        int total = accumulator.getTotal();

        // Verify
        assertEquals(165, firstSum);
        assertEquals(165, total);
    }

    @Test
    public void WHEN_multiple_accumulations_in_succession_are_supplied_THEN_the_accumulation_sums_should_total_AND_the_running_total_should_be_following() {

        // Exercise 1
        int firstSum = accumulator.accumulate(1, 2, 3);

        // Verify 1
        assertEquals(6, firstSum);
        assertEquals(6, accumulator.getTotal());

        // Exercise 2
        int secondSum = accumulator.accumulate(4);

        // Verify 2
        assertEquals(4, secondSum);
        assertEquals(10, accumulator.getTotal());

        // Exercise 3
        int thirdSum = accumulator.accumulate(44,55,66);

        // Verify 3
        assertEquals(165, thirdSum);
        assertEquals(175, accumulator.getTotal());
    }

    @Test
    public void WHEN_a_reset_is_applied_before_any_accumulations_THEN_then_the_running_total_should_remain_as_0() {

        // Exercise
        accumulator.reset();

        // Verify
        assertEquals(0, accumulator.getTotal());

    }

    @Test
    public void WHEN_a_reset_is_applied_after_an_accumulation_THEN_then_the_running_total_should_be_reset_to_0() {

        // Setup
        accumulator.accumulate(4,9);

        // Pre-verify
        assertEquals(13, accumulator.getTotal());

        // Exercise
        accumulator.reset();

        // Verify
        assertEquals(0, accumulator.getTotal());

    }

    @Test
    public void WHEN_a_reset_is_applied_multiple_times_during_multiple_accumulations_THEN_then_the_running_total_should_be_reset_to_0_each_time() {

        // Pre-verify
        accumulator.accumulate(4,9);
        accumulator.accumulate(500,9600);
        assertEquals(10113, accumulator.getTotal());

        // Exercise 1
        accumulator.reset();
        // Verify 1
        assertEquals(0, accumulator.getTotal());

        // Exercise 2
        accumulator.accumulate(6);
        accumulator.accumulate(1,1,1,1);
        assertEquals(10, accumulator.getTotal());
        accumulator.reset();

        // Verify 2
        assertEquals(0, accumulator.getTotal());

    }


}
