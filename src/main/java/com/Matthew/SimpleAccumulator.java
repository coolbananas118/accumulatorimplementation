package com.Matthew;

import java.util.stream.IntStream;

/**
 * Created by matthew2chambers on 03/10/2016.
 */
public class SimpleAccumulator implements Accumulator {

    private int runningTotal = 0;

    @Override
    public int accumulate(int... values) {
        int accumulatedAmount = IntStream.of(values).parallel().sum();
        runningTotal += accumulatedAmount;
        return accumulatedAmount;
    }

    @Override
    public int getTotal() {
        return runningTotal;
    }

    @Override
    public void reset() {
        runningTotal = 0;
    }
}
